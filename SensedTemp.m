%{

Author: Rafa� Osadnik
Institute of Physics
Technical physics

A function that calculates heat index in degrees Celsius

based of formula from: https://www.wpc.ncep.noaa.gov/html/heatindex_equation.shtml

%}

function result = SensedTemp(tempC,relHum)
  
  tempF = tempC * 9/5 + 32;
  
  
  heatIndex = 0.5 *(tempF + 61 + (1.2*(tempF - 68)) + (relHum*0.094));
  
  if(heatIndex >= 80)
  
     heatIndex = -42.379 +2.04901523 * tempF + 10.14333127*relHum - 0.22475541*tempF*relHum -...
      0.00683783*tempF^2 - 0.05481717*relHum^2 + 0.00122874*tempF^2*relHum +...
      0.00085282*tempF*relHum^2 - 0.00000199*tempF^2*relHum^2;
  
      if(relHum < 13 && tempF > 80 && tempF < 112);
    
        heatIndex = heatIndex - ((13 - relHum)/4)*sqrt((17 - abs(tempF - 95)/17));
        
      elseif(relHum > 85 && tempF > 80 && tempF < 87);
      
        heatIndex = heatIndex + ((relHum - 85)/10)*((87 - tempF)/5);
      
     end
    
  end
  
  result = (heatIndex - 32)*5/9;
  
endfunction
