%{
Author: Rafa� Osadnik

Institute of Physics

Engineering Physics

Matlab in physics applications

%}

addpath('packages\jsonlab-master'); %All additional packages have to be located in this folder

stationName = {'katowice' 'raciborz' 'czestochowa'}; %Cell array that contains station names used to create correct URLs

howManyCycles = input('\nSpecify how many times do you want to download\nand save the data, type 0 if you want the\nprogram to run indifinitely: ');

cyclesCount = 0; %A variable used to track number of executed cycles

doesItExist = exist('weatherData.csv'); %Chcecking if the data file already exists

if(~doesItExist) %If it doesn't then create one
    
    fprintf('\n Data file doesnt exist, creating data file... \n\n');
    fileID = fopen('weatherData.csv','w');
    fclose(fileID);
  
elseif(doesItExist) %If it does then check if it is empty
    
    fileProperties = dir('weatherData.csv'); %Loading properties of the file
    
    if(fileProperties.bytes ~= 0)
        
         fileID = fopen('weatherData.csv','r');
         
         allCurrentData = textscan(fileID,'%s %s %f %f %f %f','Delimiter',';','HeaderLines',1); %If the file isn't empy then search for max/min values
         
         fclose(fileID);
         
         for k = 1:4
     
         [maxValues(k),maxValuesIndex(k)] = max(allCurrentData{k+2}); %Searching for max values and tracking their indices
         [minValues(k),minValuesIndex(k)] = min(allCurrentData{k+2});
      
         end 
         
         Days = allCurrentData{1};
         Hours = allCurrentData{2};
         
         %Printing max/min values found inside the data file
         
         fprintf('\n Maximum|minimum values registered so far:\n\n');
         fprintf('Temperature: %f|%f [*C] on %s|%s  %s|%s\n',maxValues(1),minValues(1),cell2mat(Days(maxValuesIndex(1))),...
         cell2mat(Days(minValuesIndex(1))),cell2mat(Hours(maxValuesIndex(1))),cell2mat(Hours(minValuesIndex(1))));
     
         fprintf('feel_Temperature: %f|%f [*C] on %s|%s  %s|%s\n',maxValues(2),minValues(2),cell2mat(Days(maxValuesIndex(2))),...
         cell2mat(Days(minValuesIndex(2))),cell2mat(Hours(maxValuesIndex(2))),cell2mat(Hours(minValuesIndex(2))));
     
         fprintf('Pressure: %f|%f [hPa] on %s|%s  %s|%s\n',maxValues(3),minValues(3),cell2mat(Days(maxValuesIndex(3))),...
         cell2mat(Days(minValuesIndex(3))),cell2mat(Hours(maxValuesIndex(3))),cell2mat(Hours(minValuesIndex(3))));
     
         fprintf('Humidity: %f|%f [%%] on %s|%s  %s|%s\n\n',maxValues(4),minValues(4),cell2mat(Days(maxValuesIndex(4))),...
         cell2mat(Days(minValuesIndex(4))),cell2mat(Hours(maxValuesIndex(4))),cell2mat(Hours(minValuesIndex(4))));
         
    end 
  
end


while(1) %Main program loop

    isItTime = clock; %Checking current time
    
    if(isItTime(5) == 40) % If current minutes value is equal to 40, execute the download sequence
    
    fprintf('Downloading data, this may take a while... \n \n');

    for k = 1:size(stationName,2)
    
         %Downloading and decoding JSON's from the station's URLs
            
         imgwStations(k) = loadjson(urlread(cell2mat(['https://danepubliczne.imgw.pl/api/data/synop/station/' stationName(k) '/format/json'])));

         wttrStations(k) = loadjson(urlread(cell2mat(['http://wttr.in/' stationName(k) '?format=j1'])));
     
    end

    fprintf('Done downloading data\n\n')


    for k = 1:size(stationName,2)*2 %Putting the data extracted from JSONs into correct variables
 
         if(k <= 3)
    
             Temperature(k) = str2double(wttrStations(k).current_condition{1}.temp_C);
             feelsLikeTemp(k) = str2double(wttrStations(k).current_condition{1}.FeelsLikeC);
             Pressure(k) = str2double(wttrStations(k).current_condition{1}.pressure);
             vWind(k) = str2double(wttrStations(k).current_condition{1}.windspeedKmph)/3.6;
             Humidity(k) = str2double(wttrStations(k).current_condition{1}.humidity);
             timeData{k} = datestr(wttrStations(k).current_condition{1}.observation_time,15);
             dateData{k} = datestr(wttrStations(k).weather{1}.date,29);
        
         else
      
             Temperature(k) = str2double(imgwStations(k-3).temperatura);
             Pressure(k) = str2double(imgwStations(k-3).cisnienie);
             vWind(k) = str2double(imgwStations(k-3).predkosc_wiatru);
             Humidity(k) = str2double(imgwStations(k-3).wilgotnosc_wzgledna);
             feelsLikeTemp(k) = SensedTemp(Temperature(k),Humidity(k));
             timeData{k} = [num2str(imgwStations(k-3).godzina_pomiaru) ':00'];
             dateData{k} = imgwStations(k-3).data_pomiaru;
      
         end

    end %End of for loop
    
    
    fileProperties = dir('weatherData.csv');

    fileID = fopen('weatherData.csv','a'); 

    if(fileProperties.bytes == 0) %Checking if the file is empty, if so, print the header first

         Header = {'Date','Time','Temperature_C','Feels_Like_temp_C','Pressure','Humidity', 'Wind_speed'};
         fprintf(fileID,'%s;%s;%s;%s;%s;%s;%s\n',Header{:});

    end

    for k = 1:size(stationName,2)*2 %Saving the data to weatherData.csv
  
         fprintf(fileID,'%s;%s;%.2f;%.2f;%.2f;%.2f;%.2f\n',dateData{k},timeData{k},Temperature(k),feelsLikeTemp(k),...
         Pressure(k),Humidity(k),vWind(k));
  
    end

    fclose(fileID);

    fileID = fopen('weatherData.csv','r');

    allCurrentData = textscan(fileID,'%s %s %f %f %f %f %f %f','Delimiter',';','HeaderLines',1);
         
    fclose(fileID);
         
    for k = 1:4
     
        [maxValues(k),maxValuesIndex(k)] = max(allCurrentData{k+2});
        [minValues(k),minValuesIndex(k)] = min(allCurrentData{k+2});
      
    end 
         
    Days = allCurrentData{1};
    Hours = allCurrentData{2};
         
    fprintf('\n Maximum|minimum values registered so far:\n\n');
    fprintf('Temperature: %f|%f [*C] on %s|%s  %s|%s\n',maxValues(1),minValues(1),cell2mat(Days(maxValuesIndex(1))),...
    cell2mat(Days(minValuesIndex(1))),cell2mat(Hours(maxValuesIndex(1))),cell2mat(Hours(minValuesIndex(1))));
     
    fprintf('feel_Temperature: %f|%f [*C] on %s|%s  %s|%s\n',maxValues(2),minValues(2),cell2mat(Days(maxValuesIndex(2))),...
    cell2mat(Days(minValuesIndex(2))),cell2mat(Hours(maxValuesIndex(2))),cell2mat(Hours(minValuesIndex(2))));
     
    fprintf('Pressure: %f|%f [hPa] on %s|%s  %s|%s\n',maxValues(3),minValues(3),cell2mat(Days(maxValuesIndex(3))),...
    cell2mat(Days(minValuesIndex(3))),cell2mat(Hours(maxValuesIndex(3))),cell2mat(Hours(minValuesIndex(3))));
     
    fprintf('Humidity: %f|%f [%%] on %s|%s  %s|%s\n\n',maxValues(4),minValues(4),cell2mat(Days(maxValuesIndex(4))),...
    cell2mat(Days(minValuesIndex(4))),cell2mat(Hours(maxValuesIndex(4))),cell2mat(Hours(minValuesIndex(4))));
     
    cyclesCount = cyclesCount + 1;
   
    end
    
    if(cyclesCount > 0)
    
        if(cyclesCount == howManyCycles) %'If' statement that controls the number of executed cycles and breaks the main loop if the desired number of cycles is reached
        
            break;
        
        end
    end
    
    pause(60);

end %End of main while loop

fprintf('Program has ended its operation \n');
    
    
